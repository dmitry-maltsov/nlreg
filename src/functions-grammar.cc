/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * functions-grammar.cc
 * Copyright (C) 2018 Konstantin Kozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// based on github.com:pbharrin/Genetic-Prog.git

#include <iostream>
#include <math.h>
#include <QStack>
#include "functions-grammar.h"

#ifndef ARGVAL
#define ARGVAL 0.00390625 /* 1/256 */
#endif

static int PRINT_TRACE = 0;

void set_print_trace (int arg) { PRINT_TRACE = arg; }

using namespace std;

double int_to_double(int arg) {
/*	int i_arg = ( arg < 22 ) ? (int)floor(exp(arg)) : (int)floor(exp(arg % 21));
    int left;
	unsigned short left16;
	unsigned short right16;
	right16 = (unsigned short)i_arg;
	left = i_arg >> (8 * sizeof(unsigned char));
	left16 = (unsigned short)left;
	double merged = (double)left16/(double)right16;*/
	double argval = ARGVAL;
	double merged = argval * arg;
	return merged;
}

//
//		ConstNode
//
ConstNode::ConstNode() {
	numChildren = 0;
	constVal = new double(rand()/(double)RAND_MAX);
	char str[20] = "";
	sprintf(str, "Const: %f", *constVal);
	label = str;
	precendence = -1;
}

ConstNode::ConstNode(int preSetVal) {
	numChildren = 0;
	constVal = new double(int_to_double(preSetVal));
	char str[20] = "";
	sprintf(str, "Const: %f", (*constVal));
	label = str;
	precendence = -1;
}

ConstNode::ConstNode(int ind, double *preSetVal) {
	numChildren = 0;
	constVal = preSetVal;
	constInd = ind;
	char str[20] = "";
	sprintf(str, "Const: %f", (*constVal));
	label = str;
	precendence = -1;
}

double ConstNode::eval(vector<double>& inVal) {
	return (*constVal);
}

/*
double ConstNode::eval(vector<double>& inVal, double& dvdt) {
	dvdt = 0;
	return constVal;
}
* */

void ConstNode::coprint() {
	cout << "(" << (*constVal) << ")";
}

ConstNode* ConstNode::prune(){
	return this;
}

ConstNode* ConstNode::clone() {
	ConstNode* retTree = new ConstNode(constInd, constVal);
	return retTree;
}

//
//		InputNode
//
InputNode::InputNode(int inputId, string pname) {
	numChildren = 0;
	scalePresent = false;
	inputIndex = inputId;
	inpname = pname;
	char str[256] = "";
	sprintf(str, "%s[%d]", pname.c_str(), inputIndex);
//	setValues(inputIndex);
	label = str;
	precendence = -1;
}

InputNode::InputNode() {
	numChildren = 1;
	scalePresent = false;
	inputIndex = -1;
	child_type[0] = 2;
	precendence = -1;
}

double InputNode::eval(vector<double>& inVal){
	if (inputIndex >= 0) {
		return inVal[inputIndex];
	} else if (children[0]) {
		return children[0]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "not defined in input" << endl;
		return 0.0;
	}
}

void InputNode::coprint(){
	if (inputIndex >= 0) {
		if (scalePresent) {
			cout << "((" << inpname << "-" << offset << ")/" << scale << ")";
		} else {
			cout << inpname;
		}
	} else if (children[0]) {
		children[0]->coprint();
	} else {
		if (PRINT_TRACE > 1) cerr << "not defined in input coprint" << endl;
		cout << "(0)";
	}
}

//void InputNode::setValues(int inIndex){
//	char str[20] = "";
//	sprintf(str, "InputVal: %d", inIndex);
//	label = str;
//}

InputNode* InputNode::prune(){
	if (inputIndex < 0 && !children[0]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

InputNode* InputNode::clone(){
	InputNode* retTree = new InputNode(inputIndex, inpname);
	return retTree;
}

//
//		Add
//
Add::Add(){
	numChildren = 2;
	child_type[0] = 0;
	child_type[1] = 0;
	label = "Add";
	precendence = 6;
}

double Add::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return children[0]->eval(inVal) + children[1]->eval(inVal);
	} else if (children[0]){
		return children[0]->eval(inVal);
	} else if (children[1]){
		return children[1]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in add"<<endl;
		return 0.0;
	}
}

/*
double Add::eval(vector<double>& inVal, double& dvdt){
	if (children[0] && children[1]){
		double dvdt1, dvdt2, ff;
		ff = children[0]->eval(inVal, dvdt1) + children[1]->eval(inVal, dvdt2);
		dvdt1 = dvdt1 + dvdt2;
		return ff;
	}
	else {
		cerr << "left and right not defined in add"<<endl;
		return -1.0;
	}
}
*/

void Add::coprint(){
	if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << " + ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " (";
		children[0]->coprint();
		cout << ") ";
	} else if (children[1]){
		cout << " (";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in add"<<endl;
		cout << " (0";
		cout << ") ";
	}
}

Add* Add::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

Add* Add::clone(){
	Add* retNode = new Add();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Subtract
//
Subtract::Subtract(){
	numChildren = 2;
	child_type[0] = 0;
	child_type[1] = 0;
	label = "Subtract";
	precendence = 6;
}

double Subtract::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return children[0]->eval(inVal) - children[1]->eval(inVal);
	} else if (children[0]){
		return children[0]->eval(inVal);
	} else if (children[1]){
		return children[1]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in subtract"<<endl;
		return 0.0;
	}
}

void Subtract::coprint(){
	if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << " - ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " (";
		children[0]->coprint();
		cout << ") ";
	} else if (children[1]){
		cout << " (";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in subtract"<<endl;
		cout << " (0";
		cout << ") ";
	}
}

Subtract* Subtract::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

Subtract* Subtract::clone(){
	Subtract* retNode = new Subtract();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Multiply
//
Multiply::Multiply(){
	numChildren = 2;
	child_type[0] = 0;
	child_type[1] = 0;
	label = "Multiply";
	precendence = 5;
}

double Multiply::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return children[0]->eval(inVal) * children[1]->eval(inVal);
	} else if (children[0]){
		return children[0]->eval(inVal);
	} else if (children[1]){
		return children[1]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in multiply"<<endl;
		return 0.0;
	}
}

void Multiply::coprint(){
	if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << " * ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " (";
		children[0]->coprint();
		cout << ") ";
	} else if (children[1]){
		cout << " (";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in multiply"<<endl;
		cout << " (0";
		cout << ") ";
	}
}

Multiply* Multiply::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

Multiply* Multiply::clone(){
	Multiply* retNode = new Multiply();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Divide
//
Divide::Divide() {
	numChildren = 2;
	child_type[0] = 0;
	child_type[1] = 0;
	label = "Divide";
	precendence = 5;
}

double Divide::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return children[0]->eval(inVal) / children[1]->eval(inVal);
	} else if (children[0]){
		return children[0]->eval(inVal);
	} else if (children[1]){
		return 1/children[1]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in divide"<<endl;
		return 0.0;
	}
}

/*
double Divide::eval(vector<double>& inVal, double& dvdt){
	if (children[0] && children[1]){
		double dvdt1, dvdt2, ff, ff1, ff2;
		ff1 = children[0]->eval(inVal, dvdt1);
		ff2 = children[0]->eval(inVal, dvdt2);
		ff = ff1 / ff2;
		dvdt = (dvdt1 * ff2 - ff1 * dvdt2) / dvdt2 / dvdt2;
		return ff;
	}
	else {
		cerr << "left and right not defined in divide"<<endl;
		return -1.0;
	}
}
*/

void Divide::coprint(){
	if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << " / ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " (";
		children[0]->coprint();
		cout << ") ";
	} else if (children[1]){
		cout << " (1";
		cout << " / ";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in divide"<<endl;
		cout << " (0";
		cout << ") ";
	}
}

Divide* Divide::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

Divide* Divide::clone(){
	Divide* retNode = new Divide();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		InputMinusConst
//
InputMinusConst::InputMinusConst(){
	numChildren = 2;
	child_type[0] = 2;
	child_type[1] = 1;
	label = "InputMinusConst";
	precendence = 6;
}

double InputMinusConst::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return children[0]->eval(inVal) - children[1]->eval(inVal);
	} else if (children[0]){
		return children[0]->eval(inVal);
	} else if (children[1]){
		return -children[1]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in InputMinusConst" << endl;
		return 0.0;
	}
}

/*
double InputMinusConst::eval(vector<double>& inVal, double& dvdt){
	if (children[0] && children[1]){
		double ff, dvdt1, dvdt2;
		ff = children[0]->eval(inVal, dvdt1) - children[1]->eval(inVal, dvdt2);
		dvdt = dvdt1 - dvdt2;
		return ff;
	}
	else {
		cerr << "left and right not defined in InputMinusConst" << endl;
		return -1.0;
	}
}
*/

void InputMinusConst::coprint(){
	if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << " - ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " (";
		children[0]->coprint();
		cout << ") ";
	} else if (children[1]){
		cout << " (";
		cout << " - ";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in InputMinusConst" << endl;
		cout << " (";
		cout << " 0 ";
		cout << ") ";
	}
}

InputMinusConst* InputMinusConst::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

InputMinusConst* InputMinusConst::clone(){
	InputMinusConst* retNode = new InputMinusConst();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		ConstMinusInput
//
ConstMinusInput::ConstMinusInput() {
	numChildren = 2;
	child_type[0] = 2;
	child_type[1] = 1;
	label = "ConstMinusInput";
	precendence = 6;
}

double ConstMinusInput::eval(vector<double>& inVal) {
	if (children[0] && children[1]) {
		return - children[0]->eval(inVal) + children[1]->eval(inVal);
	}
	else if (children[0]) {
		return -children[0]->eval(inVal);
	}
	else if (children[1]) {
		return +children[1]->eval(inVal);
	}
	else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in ConstMinusInput" << endl;
		return 0.0;
	}
}

void ConstMinusInput::coprint() {
	if (children[0] && children[1]) {
		cout << " (-";
		children[0]->coprint();
		cout << " + ";
		children[1]->coprint();
		cout << ") ";
	}
	else if (children[0]) {
		cout << " ( - ";
		children[0]->coprint();
		cout << ") ";
	}
	else if (children[1]) {
		cout << " (";
		children[1]->coprint();
		cout << ") ";
	}
	else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in ConstMinusInput" << endl;
		cout << " (";
		cout << " 0 ";
		cout << ") ";
	}
}

ConstMinusInput* ConstMinusInput::prune() {
	if (children[0]) {
		children[0] = children[0]->prune();
	}
	if (children[1]) {
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]) {
		//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

ConstMinusInput* ConstMinusInput::clone() {
	ConstMinusInput* retNode = new ConstMinusInput();
	for (int i = 0; i < numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		RecInputMinusConst
//
RecInputMinusConst::RecInputMinusConst(){
	numChildren = 2;
	child_type[0] = 2;
	child_type[1] = 1;
	label = "RecInputMinusConst";
	precendence = 6;
}

double RecInputMinusConst::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return 1/(children[0]->eval(inVal) - children[1]->eval(inVal));
	} else if (children[0]) {
		return 1/children[0]->eval(inVal);
	} else if (children[1]) {
		return -1/children[1]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in RecInputMinusConst" << endl;
		return 1.0;
	}
}

void RecInputMinusConst::coprint(){
	if (children[0] && children[1]){
		cout << " (1/(";
		children[0]->coprint();
		cout << " - ";
		children[1]->coprint();
		cout << ")) ";
	} else if (children[0]) {
		cout << " (1/(";
		children[0]->coprint();
		cout << ")) ";
	} else if (children[1]) {
		cout << " (-1/(";
		children[1]->coprint();
		cout << ")) ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in InputMinusConst" << endl;
		cout << " (1";
		cout << ") ";
	}
}

RecInputMinusConst* RecInputMinusConst::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

RecInputMinusConst* RecInputMinusConst::clone(){
	RecInputMinusConst* retNode = new RecInputMinusConst();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		RecConstMinusInput
//
RecConstMinusInput::RecConstMinusInput() {
	numChildren = 2;
	child_type[0] = 2;
	child_type[1] = 1;
	label = "RecConstMinusInput";
	precendence = 6;
}

double RecConstMinusInput::eval(vector<double>& inVal) {
	if (children[0] && children[1]) {
		return 1 / (- children[0]->eval(inVal) + children[1]->eval(inVal));
	}
	else if (children[0]) {
		return -1 / children[0]->eval(inVal);
	}
	else if (children[1]) {
		return 1 / children[1]->eval(inVal);
	}
	else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in RecConstMinusInput" << endl;
		return 1.0;
	}
}

void RecConstMinusInput::coprint() {
	if (children[0] && children[1]) {
		cout << " (1/(-";
		children[0]->coprint();
		cout << " + ";
		children[1]->coprint();
		cout << ")) ";
	}
	else if (children[0]) {
		cout << " (-1/(";
		children[0]->coprint();
		cout << ")) ";
	}
	else if (children[1]) {
		cout << " (1/(";
		children[1]->coprint();
		cout << ")) ";
	}
	else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in RecConstMinusInput" << endl;
		cout << " (1";
		cout << ") ";
	}
}

RecConstMinusInput* RecConstMinusInput::prune() {
	if (children[0]) {
		children[0] = children[0]->prune();
	}
	if (children[1]) {
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]) {
		//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

RecConstMinusInput* RecConstMinusInput::clone() {
	RecConstMinusInput* retNode = new RecConstMinusInput();
	for (int i = 0; i < numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Sqr
//
Sqr::Sqr(){
	numChildren = 1;
	child_type[0] = 0;
	label = "Sqr";
	precendence = 6;
}

double Sqr::eval(vector<double>& inVal){
	
	if (children[0]){
		return pow(children[0]->eval(inVal), 2);
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Sqr" << endl;
		return 0.0;
	}
}

void Sqr::coprint(){

		if (children[0]){
			cout << " (";
			children[0]->coprint();
			cout << ")^2 ";
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Sqr" << endl;
		cout << " (";
		cout << " 0 ";
		cout << ")^2 ";
	}
}

Sqr* Sqr::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (!children[0]){
		delete this;
		return NULL;
	}
	return this;
}

Sqr* Sqr::clone(){
	Sqr* retNode = new Sqr();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Sqrt
//
Sqrt::Sqrt(){
	numChildren = 1;
	child_type[0] = 0;
	label = "Sqrt";
	precendence = 6;
}

double Sqrt::eval(vector<double>& inVal){
	
	if (children[0]){
		double child = children[0]->eval(inVal);
		if(child >= 0){
			return sqrt(child);
	}
		cerr << "sqrt arg < 0" << endl;
		exit(1);
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Sqrt" << endl;
		return 0.0;
	}
}

void Sqrt::coprint(){

		if (children[0]){
			cout << " (";
			children[0]->coprint();
			cout << ")^(1/2) ";
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Sqrt" << endl;
		cout << " (";
		cout << " 0 ";
		cout << ")^(1/2) ";
	}
}

Sqrt* Sqrt::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (!children[0]){
		delete this;
		return NULL;
	}
	return this;
}

Sqrt* Sqrt::clone(){
	Sqrt* retNode = new Sqrt();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Sin
//
Sin::Sin(){
	numChildren = 1;
	child_type[0] = 0;
	label = "Sin";
	precendence = 6;
}

double Sin::eval(vector<double>& inVal){
	
	if (children[0]){
		return sin(children[0]->eval(inVal));
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Sin" << endl;
		return 0.0;
	}
}

void Sin::coprint(){

		if (children[0]){
			cout << " Sin[";
			children[0]->coprint();
			cout << "] ";
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Sin" << endl;
		cout << " Sin[";
		cout << " - ";
		cout << "]";
	}
}

Sin* Sin::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (!children[0]){
		delete this;
		return NULL;
	}
	return this;
}

Sin* Sin::clone(){
	Sin* retNode = new Sin();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Cos
//
Cos::Cos(){
	numChildren = 1;
	child_type[0] = 0;
	label = "Cos";
	precendence = 6;
}

double Cos::eval(vector<double>& inVal){
	
	if (children[0]){
		return cos(children[0]->eval(inVal));
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Cos" << endl;
		return 0.0;
	}
}

void Cos::coprint(){

		if (children[0]){
			cout << " Cos[";
			children[0]->coprint();
			cout << "] ";
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Cos" << endl;
		cout << " Cos[";
		cout << " - ";
		cout << "]";
	}
}

Cos* Cos::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (!children[0]){
		delete this;
		return NULL;
	}
	return this;
}

Cos* Cos::clone(){
	Cos* retNode = new Cos();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Log
//
Log::Log(){
	numChildren = 1;
	child_type[0] = 0;
	label = "Log";
	precendence = 6;
}

double Log::eval(vector<double>& inVal){
	
	if (children[0]){
		double child = children[0]->eval(inVal);
		if(child > 0){
			return log(child);
	}
		cerr << "Log arg <= 0" << endl;
		exit(1);
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Log" << endl;
		return 0.0;
	}
}

void Log::coprint(){

		if (children[0]){
			cout << " Log[";
			children[0]->coprint();
			cout << "] ";
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Log" << endl;
		cout << " Log[";
		cout << " - ";
		cout << "]";
	}
}

Log* Log::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (!children[0]){
		delete this;
		return NULL;
	}
	return this;
}

Log* Log::clone(){
	Log* retNode = new Log();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		Exp
//
Exp::Exp(){
	numChildren = 1;
	child_type[0] = 0;
	label = "Exp";
	precendence = 6;
}

double Exp::eval(vector<double>& inVal){
	
	if (children[0]){
		return exp(children[0]->eval(inVal));
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Exp" << endl;
		return 0.0;
	}
}

void Exp::coprint(){

		if (children[0]){
			cout << " Exp[";
			children[0]->coprint();
			cout << "] ";
	} else {
		if (PRINT_TRACE > 1) cerr << "arg not defined in Exp" << endl;
		cout << " Exp[";
		cout << " - ";
		cout << "]";
	}
}

Exp* Exp::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (!children[0]){
		delete this;
		return NULL;
	}
	return this;
}

Exp* Exp::clone(){
	Exp* retNode = new Exp();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}

//
//		InputMultConst
//
InputMultConst::InputMultConst(){
	numChildren = 2;
	child_type[0] = 2;
	child_type[1] = 1;
	label = "InputMultConst";
	precendence = 6;
}

double InputMultConst::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return children[0]->eval(inVal) * children[1]->eval(inVal);
	} else if (children[0]){
		return 0;
	} else if (children[1]){
		return 0;
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in InputMultConst" << endl;
		return 0.0;
	}
}

void InputMultConst::coprint(){
	if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << " * ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " ( 0 ) ";
	} else if (children[1]){
		cout << " ( 0 ) ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in InputMultConst" << endl;
		cout << " ( 0 ) ";
	}
}

InputMultConst* InputMultConst::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

InputMultConst* InputMultConst::clone(){
	InputMultConst* retNode = new InputMultConst();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}



//
//		WA
//
WA::WA(){
	numChildren = 3;
	child_type[0] = 1;
	child_type[1] = 2;
	child_type[2] = 2;
	label = "WA";
	precendence = 6;
}

double WA::eval(vector<double>& inVal){
	if (children[0] && children[1] && children[2]){
		return children[0]->eval(inVal) * children[1]->eval(inVal) + (1 - children[0]->eval(inVal)) * children[2]->eval(inVal);
	} else if (children[0] && children[1]){
		return children[0]->eval(inVal) * children[1]->eval(inVal);
	} else if (children[0] && children[2]){
		return (1 - children[0]->eval(inVal)) * children[2]->eval(inVal);
	} else if (children[1] && children[2]){
		return children[2]->eval(inVal);
	} else if (children[0]){
		return 0;
	} else if (children[1]){
		return 0;
	} else if (children[2]){
		return children[2]->eval(inVal);
	} else {
		if (PRINT_TRACE > 1) cerr << "error in WA" << endl;
		return 0.0;
	}
}

void WA::coprint(){
	if (children[0] && children[1] && children[2]){
		cout << " (";
		children[0]->coprint();
		cout << "*";
		children[1]->coprint();
		cout << " + (1 - ";
		children[0]->coprint();
		cout << ")*";
		children[2]->coprint();
		cout << ") ";
	} else if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << "*";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0] && children[2]){
		cout << " (";
		cout << "(1 - ";
		children[0]->coprint();
		cout << ")*";
		children[2]->coprint();
		cout << ") ";
	} else if (children[1] && children[2]){
		cout << " (";
		children[2]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " ( 0 ) ";
	} else if (children[1]){
		cout << " ( 0 ) ";
	} else if (children[2]){
		cout << " (";
		children[2]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "error in WA" << endl;
		cout << " ( 0 ) ";
	}
}

WA* WA::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (children[2]){
		children[2] = children[2]->prune();
	}
	if (!children[0] && !children[1] && !children[2]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

WA* WA::clone(){
	WA* retNode = new WA();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}



//
//		OWA
//
OWA::OWA(){
	numChildren = 3;
	child_type[0] = 1;
	child_type[1] = 2;
	child_type[2] = 2;
	label = "OWA";
	precendence = 6;
}

double OWA::eval(vector<double>& inVal){
	if (children[0] && children[1] && children[2]){
		return children[0]->eval(inVal) * max(children[1]->eval(inVal), children[2]->eval(inVal)) + (1 - children[0]->eval(inVal)) * min(children[1]->eval(inVal), children[2]->eval(inVal));
	} else if (children[0] && children[1]){
		return children[0]->eval(inVal) * max(children[1]->eval(inVal), 0.0) + (1 - children[0]->eval(inVal)) * min(children[1]->eval(inVal), 0.0);
	} else if (children[0] && children[2]){
		return children[0]->eval(inVal) * max(0.0, children[2]->eval(inVal)) + (1 - children[0]->eval(inVal)) * min(0.0, children[2]->eval(inVal));
	} else if (children[1] && children[2]){
		return min(children[1]->eval(inVal), children[2]->eval(inVal));
	} else if (children[0]){
		return 0;
	} else if (children[1]){
		return min(children[1]->eval(inVal), 0.0);
	} else if (children[2]){
		return min(0.0, children[2]->eval(inVal));
	} else {
		if (PRINT_TRACE > 1) cerr << "error in OWA" << endl;
		return 0.0;
	}
}

void OWA::coprint(){
	if (children[0] && children[1] && children[2]){
		cout << " (";
		children[0]->coprint();
		cout << "*max(";
		children[1]->coprint();
		cout << ", ";
		children[2]->coprint();
		cout << ") + (1 - ";
		children[0]->coprint();
		cout << ")*min(";
		children[1]->coprint();
		cout << ", ";
		children[2]->coprint();
		cout << ")) ";
	} else if (children[0] && children[1]){
		cout << " (";
		children[0]->coprint();
		cout << "*max(";
		children[1]->coprint();
		cout << ", 0) + (1 - ";
		children[0]->coprint();
		cout << ")*min(";
		children[1]->coprint();
		cout << ", 0))";
	} else if (children[0] && children[2]){
		cout << " (";
		children[0]->coprint();
		cout << "*max(0, ";
		children[2]->coprint();
		cout << ") + (1 - ";
		children[0]->coprint();
		cout << ")*min(0, ";
		children[2]->coprint();
		cout << ")) ";
	} else if (children[1] && children[2]){
		cout << " (";
		cout << "min(";
		children[1]->coprint();
		cout << ", ";
		children[2]->coprint();
		cout << ")) ";
	} else if (children[0]){
		cout << " ( 0 ) ";
	} else if (children[1]){
		cout << "(min(";
		children[1]->coprint();
		cout << ", 0)) ";
	} else if (children[2]){
		cout << " (";
		cout << "min(0, ";
		children[2]->coprint();
		cout << ")) ";
	}else {
		if (PRINT_TRACE > 1) cerr << "error in WA" << endl;
		cout << " ( 0 ) ";
	}
}

OWA* OWA::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (children[2]){
		children[2] = children[2]->prune();
	}
	if (!children[0] && !children[1] && !children[2]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

OWA* OWA::clone(){
	OWA* retNode = new OWA();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}



//
//		Max
//
Max::Max() {
	numChildren = 2;
	child_type[0] = 0;
	child_type[1] = 0;
	label = "Max";
	precendence = 5;
}

double Max::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return max(children[0]->eval(inVal), children[1]->eval(inVal));
	} else if (children[0]){
		return max(children[0]->eval(inVal), 0.0);
	} else if (children[1]){
		return max(0.0, children[1]->eval(inVal));
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in Max"<<endl;
		return 0.0;
	}
}

void Max::coprint(){
	if (children[0] && children[1]){
		cout << " max(";
		children[0]->coprint();
		cout << ", ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " max(";
		children[0]->coprint();
		cout << ", 0) ";
	} else if (children[1]){
		cout << " max(0, ";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in Max"<<endl;
		cout << " ( 0";
		cout << " ) ";
	}
}

Max* Max::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

Max* Max::clone(){
	Max* retNode = new Max();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}



//
//		Min
//
Min::Min() {
	numChildren = 2;
	child_type[0] = 0;
	child_type[1] = 0;
	label = "Min";
	precendence = 5;
}

double Min::eval(vector<double>& inVal){
	if (children[0] && children[1]){
		return min(children[0]->eval(inVal), children[1]->eval(inVal));
	} else if (children[0]){
		return min(children[0]->eval(inVal), 0.0);
	} else if (children[1]){
		return min(0.0, children[1]->eval(inVal));
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in Min"<<endl;
		return 0.0;
	}
}

void Min::coprint(){
	if (children[0] && children[1]){
		cout << " min(";
		children[0]->coprint();
		cout << ", ";
		children[1]->coprint();
		cout << ") ";
	} else if (children[0]){
		cout << " min(";
		children[0]->coprint();
		cout << ", 0) ";
	} else if (children[1]){
		cout << " min(0, ";
		children[1]->coprint();
		cout << ") ";
	} else {
		if (PRINT_TRACE > 1) cerr << "left and right not defined in Min"<<endl;
		cout << " (0";
		cout << ") ";
	}
}

Min* Min::prune(){
	if (children[0]){
		children[0] = children[0]->prune();
	}
	if (children[1]){
		children[1] = children[1]->prune();
	}
	if (!children[0] && !children[1]){
//		cout << *this << endl;
		delete this;
		return NULL;
	}
	return this;
}

Min* Min::clone(){
	Min* retNode = new Min();
	for (int i=0; i<numChildren; i++) {
		retNode->children[i] = children[i]->clone();
	}
	return retNode;
}
/*
 * expr = grule(op(expr, expr), X, (X - Y), 1/(X - Y)),
 * Y = grule(0.1, 0.3, 0.5, 0.7, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0),
 * op = grule(`+`, `-`, `*`, `/`),
 * X = grule(@PREDICTORS@)

 * expr = grule((expr + expr), (expr - expr), (expr * expr), (expr / expr), X, (X - Y), 1/(X - Y)),
 * Y = grule(0.1, 0.3, 0.5, 0.7, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0),
 * X = grule(@PREDICTORS@)


 */


GrammarNode* GrammarContainer::find_node_type_0(int gen, double *phenotype, std::map<string, int> funcCodes)
{
	GrammarNode*n;
	int ge = gen % n_nodes_type_0;
	if (ge == funcCodes["INPUTNODE"])
		n = new InputNode();
	else if (ge == funcCodes["ADD"])
		n = new Add();
	else if (ge == funcCodes["SUBTRACT"])
		n = new Subtract();
	else if (ge == funcCodes["MULTIPLY"])
		n = new Multiply();
	else if (ge == funcCodes["DIVIDE"])
		n = new Divide();
	else if (ge == funcCodes["INPUTMINUSCONST"]) {
		this->last_const = 0;
		n = new InputMinusConst();
	}
	else if (ge == funcCodes["CONSTMINUSINPUT"]) {
		this->last_const = this->n_predictors;
		n = new ConstMinusInput();
	}
	else if (ge == funcCodes["RECINPUTMINUSCONST"]) {
		this->last_const = 0;
		n = new RecInputMinusConst();
	}
	else if (ge == funcCodes["RECCONSTMINUSINPUT"]) {
		this->last_const = this->n_predictors;
		n = new RecConstMinusInput();
	}
	else if (ge == funcCodes["INPUTMULTCONST"]) {
		this->last_const = 2*this->n_predictors;
		n = new InputMultConst();
	}
	else if (ge == funcCodes["SQR"])
		n = new Sqr();
	else if (ge == funcCodes["SQRT"])
		n = new Sqrt();
	else if (ge == funcCodes["SIN"])
		n = new Sin();
	else if (ge == funcCodes["COS"])
		n = new Cos();
	else if (ge == funcCodes["LOG"])
		n = new Log();
	else if (ge == funcCodes["EXP"])
		n = new Exp();
	else if (ge == funcCodes["Max"])
		n = new Max();
	else if (ge == funcCodes["Min"])
		n = new Min();
	else if (ge == funcCodes["WA"]) {
		this->last_const = 3*this->n_predictors;
		n = new WA();
	}
	else if (ge == funcCodes["OWA"]) {
		this->last_const = 3*this->n_predictors;
		n = new OWA();
	}
	else {
		cout << "Error find node type 0" << gen << endl;
	}
	(*phenotype) = (double)ge;
	return n;
}

GrammarNode* GrammarContainer::find_node_type_1(int gen, double conc, double *phenotype)
{
	GrammarNode*n;
	(*phenotype) = conc;
	n = new ConstNode(gen, phenotype);
	return n;
}

GrammarNode* GrammarContainer::find_node_type_2(int gen, double *phenotype)
{
	GrammarNode*n;
	int ge = gen % n_predictors;
	this->last_predictor = ge + this->last_const;
	n = new InputNode(ge, predictors[ge]);
	(*phenotype) = (double)ge;
	return n;
}

GrammarNode* GrammarContainer::find_node(int type, int gen, double conc, double *phenotype, int& phenomask, std::map<string, int> funcCodes)
{
	GrammarNode*n;
	switch (type) {
		case 0:
			phenomask = 0;
			n = find_node_type_0(gen, phenotype, funcCodes);
			break;
		case 1:
			phenomask = 1;
			n = find_node_type_1(gen, conc, phenotype);
			break;
		case 2:
			phenomask = 2;
			n = find_node_type_2(gen, phenotype);
			break;
		default:
			cout << "Error find node " << type << endl;
	}
	return n;
}

/* type:
 * 0 - expr
 * 1 - const
 * 2 - predictor
 * (3 - operation?)
 */

GrammarNode* GrammarContainer::build_tree(vector<int>& genotype, vector<double>& conc, double *phenotype, int *phenomask, std::map<string, int> funcCodes)
{
	struct StackNode {
		GrammarNode*kernel;
		int side;
		int child_type;
		StackNode(GrammarNode*orig, int p, int ct) {
			kernel = orig;
			side = p;
			child_type = ct;
		}
	};
	GrammarNode*root_expr, *child;
	StackNode *curr;
	QStack<StackNode*> stack;
	this->last_predictor = 0;
	this->last_const = 0;
	int i = 0;
	int curr_type = 0;

	root_expr = find_node(curr_type, genotype[i], conc[this->last_predictor], &phenotype[i], phenomask[i], funcCodes);
	i++;
//	cout << "g " << genotype[i - 1] << ' ' << i - 1 << ' ' << *root_expr << endl;

	if (root_expr->numChildren == 3) {
		stack.push(new StackNode(root_expr, 1, root_expr->child_type[1]));
		stack.push(new StackNode(root_expr, 2, root_expr->child_type[2]));
	}
	if (root_expr->numChildren == 2) {
		stack.push(new StackNode(root_expr, 1, root_expr->child_type[1]));
	}
	if (root_expr->numChildren > 0) {
		stack.push(new StackNode(root_expr, 0, root_expr->child_type[0]));
	}
	i = (i < genotype.size()) ? i : 0 ;
	while(!stack.isEmpty() && i < genotype.size()) {
		curr = stack.pop();
		curr_type = curr->child_type;
		child = find_node(curr_type, genotype[i], conc[this->last_predictor], &phenotype[i], phenomask[i], funcCodes);
		i++;
//		cout << "g " << genotype[i - 1] << ' ' << i - 1 << ' ' << *child << endl;
		curr->kernel->children[curr->side] = child;
		if (child->numChildren == 2) {
			stack.push(new StackNode(child, 1, child->child_type[1]));
		}
		if (child->numChildren == 3) {
			stack.push(new StackNode(child, 1, child->child_type[1]));
			stack.push(new StackNode(child, 2, child->child_type[2]));
		}
		if (child->numChildren > 0) {
			stack.push(new StackNode(child, 0, child->child_type[0]));
		}
	}

	while(!stack.isEmpty()) {
		curr = stack.pop();
		curr->kernel->children[curr->side] = NULL;
	}

	root_expr = root_expr->prune();
	if (!root_expr) root_expr = new ConstNode(-1, new double(0.0));
	return root_expr;
}

void GrammarContainer::build_nth_tree(vector<int>& genotype, vector<double>& conc, int n, double *phenotype, int *phenomask)
{
	tree[n] = build_tree(genotype, conc, phenotype, phenomask, this->func_codes_1);
}
