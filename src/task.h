/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * task.h
 * Copyright (C) 2018 Konstantin Kozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TASK_H_
#define _TASK_H_

#include <string>
#include <iostream>
#include <QtCore>

#include <highfive/H5Attribute.hpp>
#include <highfive/H5File.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>

#include <armadillo>
#include "nlreg.h"

using namespace std;
using namespace HighFive;

class Task : public QObject
{
    Q_OBJECT
private:
	QString h5_file_name;
	int PRINT_TRACE;
	bool extra_covar;
	std::vector<std::string> measurements;
	std::vector<std::string> species;
	std::vector<std::vector<double> > vec, resp;
	int nSamples;
	int ns;
	int nPredictors;
	int nn;

	std::vector<std::vector<double> > gr_covar;
	std::vector<std::string> gr_names;
	int nGrCovar;

	arma::vec response;

	arma::vec solution;
	Nlreg *nl;

	double run_training(arma::vec& solution) {
		double training_error;
		if (extra_covar) {
			for (size_t j = 0; j < nSamples; j++) {
				solution(j) = nl->get_func_value(vec[j], gr_covar[j]);
			}
		} else {
			for (size_t j = 0; j < nSamples; j++) {
				solution(j) = nl->get_func_value(vec[j]);
			}
		}
		training_error = norm(solution - response, 2);
//		training_error = cor(solution, response)(0,0);
		if (PRINT_TRACE > 0) {
			for (size_t j = 0; j < nSamples; j++) {
				cout << j << " " << response(j) << " " << solution(j) << " " << endl;
			}
		}
		return training_error;
	}
	arma::mat std2arvec(std::vector<std::vector<double> > &vec, int n_rows, int offset) {
		arma::vec Y(n_rows, 1);
		for (size_t i = 0; i < n_rows; ++i) {
			Y(i) = vec[i][offset];
		}
		return Y;
	}

public:
	Task(QString file_name, QString func_file_name, int nF, int wL, bool ecovar = false, int print_trace = 0, QObject *parent = 0, std::vector<int> *use_func = NULL, std::vector<int> *exclude_func = NULL) : QObject(parent), h5_file_name(file_name) {
/*		std::cout << "Got " << h5_file_name.toStdString() << " file" << std::endl;
		std::cout << "Got " << funcs_file_name.toStdString() << " funcs" << std::endl;*/
		PRINT_TRACE = print_trace;
		extra_covar = ecovar;
// Load data
		nSamples = 100;
		ns = -1;
		nn = -1;
		nGrCovar = 0;
	    try {
		    File file(h5_file_name.toStdString(), File::ReadOnly);
			DataSet a0_read = file.getDataSet("data");
			DataSpace space = a0_read.getSpace();
			nSamples = space.getDimensions()[0]; // number of samples
			nPredictors = space.getDimensions()[1]; // number of measurements
			a0_read.read(vec);
			DataSet b_read = file.getDataSet("response");
			space = b_read.getSpace();
			ns = space.getDimensions()[0]; // number of samples
			nn = space.getDimensions()[1]; // number of measurements
			b_read.read(resp);
			assert(ns == nSamples);
			assert(nn == 1);
			DataSet a1_read = file.getDataSet("species");
			a1_read.read(species);
			DataSet a2_read = file.getDataSet("measurements");
			a2_read.read(measurements);
			if (extra_covar) {
				DataSet c0_read = file.getDataSet("gr_covar");
				DataSpace space = c0_read.getSpace();
				nGrCovar = space.getDimensions()[1]; // number of measurements
				c0_read.read(gr_covar);
				DataSet c1_read = file.getDataSet("gr_names");
				c1_read.read(gr_names);
			}
	    } catch (Exception& err) {
    		std::cerr << err.what() << std::endl;
		}
		response = std2arvec(resp, ns, 0);
		nl = new Nlreg(func_file_name, measurements, gr_names, nF, wL, 1, print_trace, use_func, exclude_func);
		nl->nlreg_build();
	}
public slots:
    void run_wi()
    {
// Train coefficients
		solution = arma::vec(nSamples);
		double training_error;
		training_error = run_training(solution);
// Print results
		cout << "terror=" << training_error << endl;
		cout << "l1pen=" << nl->get_l1_pen() << endl;
		cout << "l2pen=" << nl->get_l2_pen() << endl;
		if (PRINT_TRACE > 0) {
			nl->print_trace(0);
		}
		emit finished();
	}
signals:
	void finished();
};

#endif // _TASK_H_
