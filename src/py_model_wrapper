#!/usr/bin/python2

"""Show GPU info"""

from tensorflow.python.client import device_lib
device_lib.list_local_devices()

"""### Import modules"""

import numpy as np
import pandas as pd
from sklearn import metrics
import random

import tensorflow as tf
import keras
from sklearn.metrics import r2_score as r2_metric
from keras import optimizers
from keras import backend as K

from scipy.stats.stats import pearsonr

import getopt, sys

try:
	opts, args = getopt.getopt(sys.argv[1:], "a:b:e:hl:n:o:p:s:v", ["help", "output="])
except getopt.GetoptError:
	# print help information and exit:
	print("Help!")
	sys.exit(2)
output = None
verbose = False
a_feature = 0
b_feature = 13
nCells = 24
eps = 1e-2
epochs = 150
bsize=25
lrate=0.035
for o, a in opts:
	if o == "-v":
		verbose = True
	if o in ("-h", "--help"):
		print("Help!")
		sys.exit()
	if o in ("-o", "--output"):
		output = a
	if o in ("-a", "--afeature"):
		a_feature = a
	if o in ("-b", "--bfeature"):
		b_feature = a
	if o in ("-l", "--lrate"):
		lrate = a
	if o in ("-n", "--ncells"):
		nCells = a
	if o in ("-p", "--epochs"):
		epochs = a
	if o in ("-s", "--batchsize"):
		bsize = a
	if o == "-e":
		eps = a

df = pd.read_csv(args[0])

a_feature = int(a_feature)
b_feature = int(b_feature)

nfeatures = b_feature - a_feature
data = df.iloc[:,a_feature:b_feature].values
target = df['state'].values
train_input, train_output = data, target
test_input, test_output = data, target
model = keras.Sequential()
model.add(keras.layers.Dense(nCells, input_dim=nfeatures, activation=tf.nn.sigmoid))
model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))
### Keras model compile"""
sgd = optimizers.SGD(lr=lrate)
model.compile(
	loss='mse',
	optimizer=sgd,
	metrics=['mse', 'mae'])
##Model summary shows the architecture of the keras model"""
## Results"""
history = model.fit(
	    train_input, train_output,
	    epochs=epochs,
	    verbose=0,
	    batch_size=bsize,
	    validation_split=eps)
loss, mse, mae = model.evaluate(test_input, test_output, verbose=0)
predicted_blossum = model.predict(data, verbose=0)
df.insert(1, 'new', predicted_blossum)
blossum_dates_predicted = df.loc[df['new'] >= 0.6]
blossum_dates_predicted = blossum_dates_predicted.loc[blossum_dates_predicted.groupby('nsam')['new'].idxmin()]
errvec = blossum_dates_predicted['d'].values - blossum_dates_predicted['event_day'].values
print(np.mean(np.abs(errvec)))
if (verbose == False):
	f = open('output.txt','w')
	f.write("MSE=" + str(mse) + '\n')
	f.write("MAE=" + str(mae) + '\n')
	f.write("Loss=" + str(loss) + '\n')
	df.to_csv('fuemod.csv')
	blossum_dates_predicted.to_csv('dates_predicted.csv')
	model.save('blossum_model.h5')
	f.write("max=" + str(np.max(errvec)) + '\n')
	f.write("min=" + str(np.min(errvec)) + '\n')
	f.write("abs=" + str(np.mean(np.abs(errvec))) + '\n')
	f.write("pearsonr=" + str(pearsonr(blossum_dates_predicted['d'].values, blossum_dates_predicted['event_day'].values)) + '\n')
	f.close()
