#!/usr/bin/Rscript --silent

library(methods)
library(h5)

comp_ssres <- function(Ytest, Ytrain) {
    score_mn <- sum((Ytest - Ytrain)^2)
    return(score_mn)
}

comp_rmse <- function(Ytest, Ytrain) {
    score_mn <- 1 - (sum((Ytest - Ytrain)^2))/(sum((Ytrain - mean(Ytrain))^2))
    return(score_mn)
}

comp_rmse_ext <- function(Ytest, Ytrain) {
    mYtrain = mean(Ytrain)
    sum1 <- sum((Ytrain - mYtrain) * (Ytest - mYtrain))
    sum2 <- sum((Ytrain - mYtrain)^2)
    sum3 <- sum((Ytest - mYtrain)^2)
    score_mn <- sum1^2/(sum2 * sum3)
    return(score_mn)
}


args <- commandArgs(trailingOnly = TRUE)

dfile <- args[[1]]
#dfile <- "chickpea-Diyarbakir.2.h5"
file <- h5file(dfile)

data <- file["data"]
response <- file["response"]
measurements <- file["measurements"]
species <- file["species"]
data <- readDataSet(data)
response <- readDataSet(response)
measurements <- readDataSet(measurements)
species <- readDataSet(species)

j <- 1
for(i in measurements) {
  assign(i, data[,j])
  j <- j + 1
}

model_res <- readLines(args[[2]])

n_funcs = grep("^F=", model_res) - 3

N <- length(response)

#cat('n funcs', n_funcs, '\n')

cN_dl <- do.call("c", lapply(1:(n_funcs - 1), FUN=function(i) { grepl("dl_", model_res[i]) }) )
cN_dl <- which(cN_dl)
cN_temp <- do.call("c", lapply(1:(n_funcs - 1), FUN=function(i) { grepl("t_", model_res[i]) }) )
cN_temp <- which(cN_temp)
cN_rrr <- do.call("c", lapply(1:(n_funcs - 1), FUN=function(i) { grepl("rrr_", model_res[i]) }) )
cN_rrr <- which(cN_rrr)
cN_u <- do.call("c", lapply(1:(n_funcs - 1), FUN=function(i) { grepl("u_", model_res[i]) }) )
cN_u <- which(cN_u)

betas = unlist(strsplit(model_res[(length(model_res) - 1)], " ", fixed = TRUE))
betas = betas[(length(betas) - n_funcs + 1):length(betas)]
betas <- gsub("-", "", betas, fixed = TRUE)
#betas

weights <- gsub("  ", " ", gsub("   ", " ", gsub("    ", " ", model_res[(n_funcs + 9)], fixed = TRUE), fixed = TRUE), fixed = TRUE)
weights = strsplit(weights, " ", fixed = TRUE)
weights = unlist(weights)#, fixed = TRUE))
weights <- as.numeric(weights[-1])
#weights

sw = sqrt(weights)

model_str = model_res[grep("^F=", model_res)]
response_model <- eval(parse(text = model_str))
SS_res_full <- comp_ssres(sw * response_model, sw * response)
R2 <- comp_rmse(sw * response_model, sw * response)
eR2 <- comp_rmse_ext(sw * response_model, sw * response)
r2 = cor.test(sw * response_model, sw * response)
pR2 = r2$estimate^2
pR2_crit = r2$p.value

alpha = 0.01
K = sum(betas > 0)
V1 = K
V2 = (N - K - 1)

pR2adj <- 1 - (1 - pR2) * (N - 1) / (N - K - 1)

pF_stat = V2 * R2/((1 - R2) * V1)
pF_crit = pf(pF_stat, V1, V2, lower.tail = FALSE)

rF_stat = V2 * pR2/((1 - pR2) * V1)
rF_crit = pf(rF_stat, V1, V2, lower.tail = FALSE)

eF_stat = V2 * eR2/((1 - eR2) * V1)
eF_crit = pf(eF_stat, V1, V2, lower.tail = FALSE)

#cat('F R2 = ', R2, "Pearson's r^2=", r2$estimate^2, "p.value=", r2$p.value, "ext. det.coef =", eR2, pF_stat, pF_crit, eF_stat, eF_crit, rF_stat, rF_crit, '\n')

predictors <- gsub("+", " ", gsub("-", " ", gsub("*", " ", gsub("(", " ", gsub(")", " ", gsub("F", "", gsub("=", "", model_str, fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE)
predictors <- strsplit(predictors, " ", fixed = TRUE)
predictors <- measurements[!is.na(match(measurements, unlist(predictors)))]
predictors <- unlist(predictors)

coefsindset <- 1:(n_funcs - 1)
cncoefs <- list(DL = cN_dl, TEMP = cN_temp, RRR = cN_rrr, U = cN_u)
cncnames <- c("DL", "TEMP", "RRR", "U")
cpat <- c("^dl_", "^t_", "^rrr_", "^u_")

res_names <- c("")
pR2_list <- c()
pR2_crit_list <- c()
rF_stat_list <- c()
rF_crit_list <- c()
me_diff_pp_list = c()
md_diff_pp_list = c()

pR2_list  <- c(pR2_list, pR2)
pR2_crit_list <- c(pR2_crit_list, pR2_crit)
rF_stat_list <- c(rF_stat_list, rF_stat)
rF_crit_list <- c(rF_crit_list, rF_crit)

N_up = 0
N_down = 0

k = 1
for (CL in cncoefs) {
  me_diff_pp = 0
  md_diff_pp = 0
  N_up = 0
  N_down = 0
  Rp2 = 0
  Rss = 0
  eRp2 = 0
  rp2 = list(estimate = 0, p.value = 1)
  pR2 = 0
  pR2_crit = 1
  nacl = cncnames[k]
  if (length(CL) > 0) {
    pmodel_str = model_str
    mpred = predictors
    for(j in 1:length(cncoefs)) {
      if (j != k) {
        for(i in cncoefs[[j]]) {
          if (as.numeric(betas[(i + 1)]) > 0) {
            pmodel_str = gsub(betas[(i + 1)], "0.0", pmodel_str, fixed = TRUE)
          }

          pp <- gsub("+", " ", gsub("-", " ", gsub("*", " ", gsub("(", " ", gsub(")", " ", gsub("F", "", gsub("=", "", model_res[i], fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE)
          pp <- strsplit(pp, " ", fixed = TRUE)
          pp <- unlist(pp)
          pp <- measurements[!is.na(match(measurements, pp))]
          pp <- unlist(pp)
          mpred = mpred[is.na(match(mpred, pp))]
        }
      }
    }
    response_model <- eval(parse(text = pmodel_str))

    mpred = unique(mpred)
    diff_pp <- diff(weights * response_model)
    diff_pp_i = rep(TRUE, times = length(diff_pp))
#    cat(mpred, diff_pp, '\n')
    for(pp in mpred) {
      diff_pp_e = diff(eval(parse(text = pp)))
      diff_pp_i = diff_pp_i & (diff_pp_e != 0)
      diff_pp[diff_pp_i] <- diff_pp[diff_pp_i]/diff_pp_e[diff_pp_i]
#      cat(pp, diff_pp, '\n')
    }
    N_up = sum(diff_pp > 0)
    N_down = sum(diff_pp < 0)
    me_diff_pp = mean(diff_pp[diff_pp_i])
    md_diff_pp = median(diff_pp[diff_pp_i])
#    cat(N_up, N_down, me_diff_pp, md_diff_pp, sd(diff_pp[diff_pp_i]), '\n')

    SS_res_reduced <- comp_ssres(sw * response_model, sw * response)
    Rss = 1 - SS_res_full/SS_res_reduced
    Rp2 <- comp_rmse(sw * response_model, sw * response)
    eRp2 <- comp_rmse_ext(sw * response_model, sw * response)
    if (sd(response_model) > 0) {
      rp2 = cor.test(sw * response_model, sw * response)
      pR2 = rp2$estimate^2
      pR2_crit = rp2$p.value
    }
    K = length(CL)
    V1 = K
    V2 = (N - K - 1)
  }
  pF_stat = V2 * Rp2/((1 - Rp2) * V1)
  pF_crit = pf(pF_stat, V1, V2, lower.tail = FALSE)

  eF_stat = V2 * eRp2/((1 - eRp2) * V1)
  eF_crit = pf(eF_stat, V1, V2, lower.tail = FALSE)

  rF_stat = V2 * pR2/((1 - pR2) * V1)
  rF_crit = pf(rF_stat, V1, V2, lower.tail = FALSE)

#  cat(nacl, "det.coef. =", Rp2, "Pearson's r^2=", rp2$estimate^2, "p.value=", rp2$p.value, "ext. det.coef =", eRp2, pF_stat, pF_crit, eF_stat, eF_crit, rF_stat, rF_crit, Rss, '\n')
  me_diff_pp_list = c(me_diff_pp_list, me_diff_pp)
  md_diff_pp_list = c(md_diff_pp_list, md_diff_pp)
  pR2_list  <- c(pR2_list, pR2)
  pR2_crit_list <- c(pR2_crit_list, pR2_crit)
  rF_stat_list <- c(rF_stat_list, rF_stat)
  rF_crit_list <- c(rF_crit_list, rF_crit)
  res_names <- c(res_names, nacl)
  k = k + 1
}

k1 = 1
for (CL1 in cncoefs) {
  nacl1 = cncnames[k1]
  if (k1 < length(cncoefs)) {
    for (k2 in (k1 + 1):length(cncoefs)) {
      me_diff_pp = 0
      md_diff_pp = 0
      N_up = 0
      N_down = 0
      CL2 = cncoefs[[k2]]
      nacl2 = cncnames[k2]
      Rp2 = 0
      eRp2 = 0
      rp2 = list(estimate = 0, p.value = 1)
      pR2 = 0
      com_cl = intersect(CL1, CL2)
      if (length(com_cl)) {
        pmodel_str = model_str
        mpred = predictors
        zero_2 = setdiff(coefsindset, com_cl)
        for(i in zero_2) {
          if (as.numeric(betas[(i + 1)]) > 0) {
            pmodel_str = gsub(betas[(i + 1)], "0.0", pmodel_str, fixed = TRUE)
          }
          pp <- gsub("+", " ", gsub("-", " ", gsub("*", " ", gsub("(", " ", gsub(")", " ", gsub("F", "", gsub("=", "", model_res[i], fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE), fixed = TRUE)
          pp <- strsplit(pp, " ", fixed = TRUE)
          pp <- unlist(pp)
          pp <- measurements[!is.na(match(measurements, pp))]
          pp <- unlist(pp)
          mpred = mpred[is.na(match(mpred, pp))]
        }
        response_model <- eval(parse(text = pmodel_str))

        mpred = unique(mpred)
        diff_pp <- diff(weights * response_model)
        diff_pp_i = rep(TRUE, times = length(diff_pp))
#        cat(mpred, diff_pp, '\n')
        for(pp in mpred) {
          diff_pp_e = diff(eval(parse(text = pp)))
          diff_pp_i = diff_pp_i & (diff_pp_e != 0)
          diff_pp[diff_pp_i] <- diff_pp[diff_pp_i]/diff_pp_e[diff_pp_i]
#          cat(pp, diff_pp, '\n')
        }
        N_up = sum(diff_pp > 0)
        N_down = sum(diff_pp < 0)
        me_diff_pp = mean(diff_pp[diff_pp_i])
        md_diff_pp = median(diff_pp[diff_pp_i])
#        cat(N_up, N_down, me_diff_pp, md_diff_pp, '\n')

        Rp2 <- comp_rmse(sw * response_model, sw * response)
        eRp2 <- comp_rmse_ext(sw * response_model, sw * response)
        if (sd(response_model) > 0) {
          rp2 = cor.test(sw * response_model, sw * response)
          pR2 = rp2$estimate^2
        }
        K = length(com_cl)
        V1 = K
        V2 = (N - K - 1)
      }
      pF_stat = V2 * Rp2/((1 - Rp2) * V1)
      pF_crit = pf(pF_stat, V1, V2, lower.tail = FALSE)

      eF_stat = V2 * eRp2/((1 - eRp2) * V1)
      eF_crit = pf(eF_stat, V1, V2, lower.tail = FALSE)

      rF_stat = V2 * pR2/((1 - pR2) * V1)
      rF_crit = pf(rF_stat, V1, V2, lower.tail = FALSE)
#      cat(nacl1, "*", nacl2, "det.coef. =", Rp2, "Pearson's r^2=", rp2$estimate^2, "p.value=", rp2$p.value, "ext. det.coef =", eRp2, pF_stat, pF_crit, eF_stat, eF_crit, rF_stat, rF_crit, '\n')
      me_diff_pp_list = c(me_diff_pp_list, me_diff_pp)
      md_diff_pp_list = c(md_diff_pp_list, md_diff_pp)
      pR2_list  <- c(pR2_list, pR2)
      pR2_crit_list <- c(pR2_crit_list, pR2_crit)
      rF_stat_list <- c(rF_stat_list, rF_stat)
      rF_crit_list <- c(rF_crit_list, rF_crit)
      res_names <- c(res_names, paste0(nacl1, "*", nacl2))
    }
  }
  k1 = k1 + 1
}
#paste(res_names)
cat(args[[2]], paste(pR2_list), pR2adj, paste(me_diff_pp_list), paste(md_diff_pp_list), paste(pR2_crit_list), paste(rF_stat_list), paste(rF_crit_list), '\n')

